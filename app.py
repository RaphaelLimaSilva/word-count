import sys
import operator

def conta_palavras(txt):

	#resp = []
	dic = {}
	for texto in txt :
		texto = texto.split(' ')
		for palavra in texto:
			if palavra not in dic and palavra != "":
				dic[palavra] = 1
			elif palavra in dic and palavra != "":
				dic[palavra] += 1


	resp = sorted(dic.items(), key=operator.itemgetter(1), reverse=True)
	return resp




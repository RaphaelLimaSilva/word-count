import unittest
import sys
from app import conta_palavras


class TestStringMethods(unittest.TestCase):

	def test(self):
		lista = conta_palavras(sys.stdin)
		ultimo = len(lista)
		cont = 0
		while cont < ultimo -1:
			self.assertGreaterEqual(lista[cont][1], lista[cont+1][1])
			cont += 1


def runTests():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestStringMethods)
    unittest.TextTestRunner(verbosity=2, failfast=True).run(suite)


if __name__ == '__main__':
	unittest.main()


